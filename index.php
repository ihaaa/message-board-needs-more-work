<?php

session_start();

require 'config.php';

require 'classes/messages.php';
require 'classes/urlhandler.php';
require 'classes/controller.php';
require 'classes/model.php';

require 'controllers/home.php';
require 'controllers/posts.php';
require 'controllers/users.php';

require 'models/home.php';
require 'models/post.php';
require 'models/user.php';

$urlhandler = new UrlHandler($_GET);
$controller = $urlhandler->createController();

if ($controller) {
	$controller->executeAction();
}