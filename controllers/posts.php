<?php

class Posts extends Controller {

	protected function index() {
		$viewmodel = new PostModel();
		$this->returnView($viewmodel->index(), true);
	}

	protected function add() {
		if(!isset($_SESSION['logged_in'])) {
			header('Location: ' . ROOT_URL . 'users/login');
		}
		$viewmodel = new PostModel();
		$this->returnView($viewmodel->add(), true);
	}

	protected function thread() {
		if(!isset($_SESSION['logged_in'])) {
			header('Location: ' . ROOT_URL . 'users/login');
		}
		$viewmodel = new PostModel();
		$this->returnView($viewmodel->thread(), true);
	}


}