<?php

class PostModel extends Model {

	public function index() {
		$this->query('SELECT * FROM posts LEFT JOIN users ON posts.user_id = users.id');
		$rows = $this->fetch();
		return $rows;
	}

	public function add() {
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$post['body'] = trim($post['body']);

		if ($_SERVER["REQUEST_METHOD"] == "POST") {

			if ($post['body'] == '' || $post['link'] == '' || $_FILES['img']['size'] == 0) {
				Messages::setMessage('Please, Fill In All Fields', 'error');
				return;
			}

			$file = $_FILES['img'];

			$fileName = $_FILES['img']['name'];
			$fileTmpName = $_FILES['img']['tmp_name'];
			$fileSize = $_FILES['img']['size'];
			$fileError = $_FILES['img']['error'];
			$fileType = $_FILES['img']['type'];

			$fileExt = explode('.', $fileName);
			$fileActualExt = strtolower(end($fileExt));

			$allowed = array('jpg', 'jpeg', 'png');

			if (in_array($fileActualExt, $allowed)) {
				if ($fileError === 0) {
					if ($fileSize < 5000000) {
						$fileNameNew = uniqid('', true) . "." . $fileActualExt;
						$fileDestination = 'images/' . $fileNameNew;
						$filePath = ROOT_PATH . $fileDestination;
						move_uploaded_file($fileTmpName, $fileDestination);
					} else {
						Messages::setMessage('The file is too big. It has to be less than 5mb.', 'error');
						return;
					}
				} else {
					Messages::setMessage('The was an error.', 'error');
					return;
				}
			} else {
				Messages::setMessage('The valid img formats are jpg, jpeg, and png.', 'error');
				return;
			}

			if (!filter_var($post['link'], FILTER_VALIDATE_URL)) {
				Messages::setMessage('Please, use a valid link.', 'error');
				return;
			}

			$this->query('INSERT INTO posts (img_path, body, link, user_id) VALUES (:img_path, :body, :link, :user_id)');
			$this->bind(':img_path', $filePath);
			$this->bind(':body', $post['body']);
			$this->bind(':link', $post['link']);
			$this->bind(':user_id', $_SESSION['user']['id']);
			$this->execute();

			$lastId = $this->lastInsertID();

			if($lastId){
				header('Location: ' . ROOT_URL . 'posts');
			}

			$this->query("
				CREATE TABLE IF NOT EXISTS chat".$lastId." (
					`chat_id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
				  `message` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
				  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`chat_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
			");
			$this->execute();

		}
		return;
	}

	public function thread() {
		$post_id = $_GET['id'];
		$thread = array();

		$this->query('SELECT * FROM posts LEFT JOIN users ON posts.user_id = users.id WHERE post_id = :post_id');
		$this->bind(":post_id", $post_id);
		$mainPost = $this->fetchOne();
		array_push($thread, $mainPost);

		// if ($_SERVER["REQUEST_METHOD"] == "POST") {
		// 	$chatMsg = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		// 	$message = trim($chatMsg['message']);

		// 	if (strlen($message) >= 1 && strlen($message) <= 240) {
		// 		$this->query("INSERT INTO chat".$post_id." (name, message) VALUES (:name, :message)");
		// 		$this->bind(':name', $_SESSION['user']['name']);
		// 		$this->bind(':message', $message);
		// 		$this->execute();	
		// 	} else {
		// 		Messages::setMessage('The max length for the messages is 240 characters', 'error');
		// 	}

		// }

		// $this->query("SELECT * FROM chat".$post_id." ORDER BY chat_id ASC");
		// $rows = $this->fetch();
		// array_push($thread, $rows);

		return $thread;
	}


}