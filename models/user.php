<?php

class UserModel extends Model {

  public function validateEmail() {
    $email = trim($_POST['email']);
    $email_pattern = "/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i";
    if(preg_match($email_pattern, $email)){
      return TRUE;
    } else {
      $this->message .= "Please enter a valid email.<br />";
      return FALSE;
    }
  }

  public function validatePassword() {
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
    if($password === $confirm_password){
      $password_pattern = "/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{4,255}$/";
      if (preg_match($password_pattern, $password)) {
        return TRUE;
      } else {
        $this->message .= "Your password has to contain a lowercase letter, an uppercase letter, a number and a special character.<br />";
        return FALSE;
      }
    }else{
      $this->message .= "The passwords do not match.<br />";
      return FALSE;
    }
  }

	public function register() {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      
      $this->query('SELECT * FROM users WHERE email = :email');
      $this->bind(':email', $_POST['email']);
      $username = $this->fetchOne();

      if ($username) {
        if ($username['email'] === $_POST['email']) {
          $this->message .= 'Email already exists<br />';
          Messages::setMessage($this->message, 'error');
          return;
        }
      }

      if (!$this->validateEmail()) {
        Messages::setMessage($this->message, 'error');
      }

      if (!$this->validatePassword()) {
        Messages::setMessage($this->message, 'error');
      }

      $name = uniqid();
      $password = hash("sha256", $_POST['password']);

      if (empty($this->message)) {

        $this->query('INSERT INTO users (name, email, password) VALUES (:name, :email, :password)');
        $this->bind(':name', $name);
        $this->bind(':email', $_POST['email']);
        $this->bind(':password', $password);

        $this->execute();

        if($this->lastInsertID()){
          header('Location: ' . ROOT_URL . 'users/login');
        }

      }
    }
	}

  public function login() {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $password = hash("sha256", $_POST['password']);

      $this->query('SELECT * FROM users WHERE email = :email AND password = :password');
      $this->bind(':email', $_POST['email']);
      $this->bind(':password', $password);

      $row = $this->fetchOne();

      if ($row) {
        $_SESSION['logged_in'] = TRUE;
        $_SESSION['user'] = array(
          "id" => $row['id'],
          "name" => $row['name'],
          "email" => $row['email']
        );
        header('Location: ' . ROOT_URL . 'posts');
      } else {
        Messages::setMessage('Incorrect login details.', 'error');
      }

    }
  }


	
}