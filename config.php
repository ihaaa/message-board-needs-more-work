<?php

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "board");
define("DB_CHAR", "utf8mb4");

define("ROOT_PATH", "/logsmvc/");
define("ROOT_URL", "http://localhost/logsmvc/");