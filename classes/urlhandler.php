<?php

class UrlHandler {
	private $controller;
	private $action;
	private $request;

	public function __construct($request){
		$this->request = $request;

		if($this->request['controller'] == ''){
			$this->controller = 'home';
		} else {
			$this->controller = $this->request['controller'];
		}

		if ($this->request['action'] == '') {
			$this->action = 'index';
		} else {
			$this->action = $this->request['action'];
		}
	}

	public function createController() {

		if (class_exists($this->controller)) {
			$parrent = class_parents($this->controller);

			if (in_array('Controller', $parrent)) {
				
				if (method_exists($this->controller, $this->action)) {
					return new $this->controller($this->action, $this->request);
				} else {
					//echo "<h1>Method does NOT exist!</h1>";
					header('Location: ' . ROOT_URL);
					return;
				}
			} else {
				//echo "<h1>Controller does NOT exist!</h1>";
				header('Location: ' . ROOT_URL);
				return;
			}
		} else {
			//echo "<h1>Controller class does NOT exist!</h1>";
			header('Location: ' . ROOT_URL);
			return;
		}

	}

}