<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Register User</h3>
  </div>
  <div class="panel-body">
    
  	<form name="register" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">
	  	<div class="form-group">
	  		<label for="email">Email:</label>
	  		<input id="email" class="form-control" type="text" name="email"  />
	  	</div>
	  	<div class="form-group">
	  		<label for="password">Password:</label>
	  		<input id="password" class="form-control" type="text" name="password"  />
	  	</div>
	  	<div class="form-group">
	  		<label for="confirm_password">Confirm Password:</label>
	  		<input id="confirm_password" class="form-control" type="text" name="confirm_password"  />
	  	</div>
			<input class="btn btn-primary" type="submit" name="submit" value="Register" />
  	</form>

  </div>
</div>