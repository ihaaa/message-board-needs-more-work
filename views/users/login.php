<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Login</h3>
  </div>
  <div class="panel-body">
    
  	<form name="login" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">
	  	<div class="form-group">
	  		<label for="email">Email:</label>
	  		<input id="email" class="form-control" type="text" name="email"  />
	  	</div>
	  	<div class="form-group">
	  		<label for="password">Password:</label>
	  		<input id="password" class="form-control" type="text" name="password"  />
	  	</div>
			<input class="btn btn-primary" type="submit" name="submit" value="Login" />
  	</form>

  </div>
</div>