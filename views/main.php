<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  
  <meta name="description" content="message board">
  <meta name="author" content="Ivan Hadzhiev">
  <link rel="icon" href="<?php echo ROOT_PATH; ?>assets/images/ant.ico">

	<title>ANTHILL</title>

	<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/style.css">

	<script src="<?php echo ROOT_PATH; ?>assets/js/jquery-3.2.1.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-default">
	  <div class="container container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo ROOT_URL; ?>"><strong>ANTHILL</strong></a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    	<ul class="nav navbar-nav">
	        <li><a href="<?php echo ROOT_URL; ?>posts">Posts</a></li>
	        <li><a href="<?php echo ROOT_URL; ?>posts/add">Add Post</a></li>
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	      	<?php if(isset($_SESSION['logged_in'])) { ?>
	        <li><a href="<?php echo ROOT_URL; ?>users/profile">Ant № [<?php echo $_SESSION['user']['name']; ?>]</a></li> <!-- add a customizeable profile page -->
	        <li><a href="<?php echo ROOT_URL; ?>users/logout">Logout</a></li>
	      	<?php } else { ?>
	      	<li><a href="<?php echo ROOT_URL; ?>users/login">Login</a></li>
	        <li><a href="<?php echo ROOT_URL; ?>users/register">Register</a></li>
	        <?php } ?>
	      </ul>
	    </div>
	  </div>
	</nav>

	<div class="container content">
		<?php Messages::displayMessage(); ?>
		<?php require "$view"; ?>
	</div>

	<footer>
    <div class="container">
      <p><img id="doge" src="<?php echo ROOT_PATH; ?>assets/images/doge.png" alt="doge" title="doge" height="56" width="56"></p>
    </div>  
  </footer>

	<script src="<?php echo ROOT_PATH; ?>assets/js/bootstrap.js"></script>
	<script src="<?php echo ROOT_PATH; ?>assets/js/main.js"></script>
</body>
</html>