<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Post Something!</h3>
  </div>
  <div class="panel-body">
    
  	<form name="addPost" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>" enctype="multipart/form-data">
  		<div class="form-group">
	  		<label for="body">Post:</label>
	  		<textarea id="body" class="form-control" name="body"></textarea>
	  	</div>
	  	<div class="form-group">		
	  		<label for="link">Link:</label>
	  		<input id="link" class="form-control" type="text" name="link"  />
			</div>
			<div class="form-group">		
	  		<label for="img">Upload Image:</label>
	  		<input id="img" class="form-control" type="file" name="img"  />
			</div>
			<input class="btn btn-primary" type="submit" name="submit" value="Post" />
			<a class="btn btn-danger" href="<?php echo ROOT_URL; ?>posts">Cancel</a>
  	</form>

  </div>
</div>