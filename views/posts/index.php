<div>

	<?php foreach($viewmodel as $item) { ?>

		<div class="well">
			<div class="file">
				<a href="<?php echo $item['img_path']; ?>" target="_blank">
					<img class="postImg" src="<?php echo $item['img_path']; ?>">
				</a>
			</div>
			<div class="postInfo">
				<span><strong>Ant: [<?php echo $item['name']; ?>]</strong></span>
				<span><?php echo $item['post_date']; ?></span>
				<span><strong>Thread: <?php echo $item['post_id']; ?></strong></span>
				<span><a class="btn btn-info btn-xs" href='<?php echo ROOT_URL . "posts/thread/" . $item['post_id']; ?>'>Join</a></span>
			</div>
			<div class="postMessage">
				<span class="postBody"><?php echo mb_strimwidth($item['body'], 0, 1200, '...'); ?></span>
				<div class="right"><a class="btn btn-default btn-xs" href="<?php echo $item['link']; ?>" target="_blank">Go To Website</a></div>
			</div>
		</div>

	<?php } ?>

</div>