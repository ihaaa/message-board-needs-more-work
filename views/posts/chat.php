<?php
	session_start();

	require '../../config.php';
	require '../../classes/model.php';
	require '../../models/post.php';

	$thread = $_POST['thread'];
	$action = $_POST['action'];

	$init = new PostModel();
		
	if ($action == 'sendMessage') {
		$message = $_POST['message'];

		if (strlen($message) >= 0 && strlen($message) <= 240) {
			$init->query("INSERT INTO chat".$thread." (name, message) VALUES (:name, :message)");
			$init->bind(':name', $_SESSION['user']['name']);
			$init->bind(':message', $message);
			$init->execute();

			$lastId = $init->lastInsertID();
			if($lastId){
				echo 1;
				exit();
			}
		} else {
			Messages::setMessage('The max length for the messages is 240 characters', 'error');
		}

	} elseif ($action == 'getMessages') {
		$init->query('SELECT * FROM chat'. $thread .' ORDER BY chat_id ASC');
		$rows = $init->fetch();

		$chatLine = "";
		foreach ($rows as $value) {
			$chatLine .= "<div id='chatData'><span>" . $value['name'] . ": </span><span><strong>" . $value['message'] . "</strong></span><span style='float:right'>" . date('H:i', strtotime($value['date'])) . "</span></div>";
		}

		echo $chatLine;

	}