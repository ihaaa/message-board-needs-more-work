<div>

		<div class="well">
			<div class="file">
				<a href="<?php echo $viewmodel[0]['img_path']; ?>" target="_blank">
					<img class="postImg" src="<?php echo $viewmodel[0]['img_path']; ?>">
				</a>
			</div>
			<div class="postInfo">
				<span><strong>Ant: [<?php echo $viewmodel[0]['name']; ?>]</strong></span>
				<span><?php echo $viewmodel[0]['post_date']; ?></span>
				<span><strong>Thread: <?php echo $viewmodel[0]['post_id']; ?></strong></span>
			</div>
			<div class="postMessage">
				<span class="postBody"><?php echo $viewmodel[0]['body']; ?></span>
				<div class="right"><a class="btn btn-default btn-xs" href="<?php echo $viewmodel[0]['link']; ?>" target="_blank">Go To Website</a></div>
			</div>
		</div>

		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title"><strong>Thread: <?php echo $viewmodel[0]['post_id']; ?></strong></h3>
		  </div>
		  <div class="panel-body">

		  	<div id="chat"></div>
		    
		  	<form name="messageForm" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>" id="messageForm">
		  		<div class="form-group">
 			
		  			<textarea id="message" class="form-control textarea" name="message" placeholder="Enter message" rows="3"></textarea>

			  	</div>
		  	</form>

		  </div>
		</div>

</div>
<script>
	$(document).ready(function() {
		var threadId = <?php echo $viewmodel[0]['post_id']; ?>;

		LoadChat();
		setInterval(function() {
			LoadChat();
		}, 1000);

		function LoadChat() {
			$.post('<?php echo ROOT_PATH; ?>views/posts/chat.php', {thread: threadId, action: "getMessages"}, function(response) {

				var scrollpos = $('#chat').scrollTop();
				var scrollpos = parseInt(scrollpos) + 520;
				var scrollHeight = $('#chat').prop('scrollHeight');

				$('#chat').html(response);

				if (scrollpos < scrollHeight) {

				} else {
					$('#chat').scrollTop($("#chat").prop('scrollHeight'));
				}
				
			});
		};

		$('.textarea').keyup(function(e){
			//alert(e.which);//to check the asci code of the enter key
			if (e.which == 13) {
				$('form').submit();
			}
		});

		$('#messageForm').submit(function() {
			var message = $('.textarea').val();
			$.post('<?php echo ROOT_PATH; ?>views/posts/chat.php', {thread: threadId, action: 'sendMessage', message: message}, function(response){
				if (response == 1) {
					LoadChat();
					document.getElementById('messageForm').reset();
				}
			});
			return false;
		});
	});
</script>